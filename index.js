const express = require("express");
const cors = require("cors");
const app = express();
const axois = require("axios");
const querystring = require("querystring");
const $ = require("cheerio");
const bodyParser = require("body-parser");
const mongodb = require("mongodb");

const MONGO_DB_URL = process.env.MONGO_DB_NEXUS_URL;
const NUMBERS_API_ENDPOINT = "/api/numbers";
const MONGO_DATABASE_NAME = "post-tracking";
const COLLECTION_NUMBERS = "numbers";
const COLLECTION_USERS = "users";

const PORT = 5206;
let db;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.options("*", cors());

///// authentication middleware
const autneticationMiddleware = async (req, res, next) => {
  try {
    const user = await db
      .collection(COLLECTION_USERS)
      .findOne({ email: req.headers.auth });
    if (user) {
      req.userEmail = user.email;
      next();
    } else {
      throw "Unauthorized access";
    }
  } catch (error) {
    res.status(401).json({ error });
  }
};

// allow access to all request to this endpoint (non registered uses)
app.get("/api/track", cors({ origin: "*" }), async (req, res) => {
  const trackingNumber = req.query.number;
  const trackingrResult = await track(trackingNumber);
  if (Object.keys(trackingrResult).length) {
    res.status(200).send(trackingrResult);
  } else {
    res.status(400).send({ message: "Tracking data is not found" });
  }
});

app.get("/api/about", cors(), (req, res) =>
  res.send(
    JSON.stringify({
      message: "This is simple API for getting tracking data from Poste Srpske",
      version: "v0.2.3"
    })
  )
);

app.get(
  NUMBERS_API_ENDPOINT,
  cors(),
  autneticationMiddleware,
  async (req, res) => {
    const numbers = await db
      .collection(COLLECTION_NUMBERS)
      .find({ email: req.userEmail })
      .toArray();
    res.status(200).send(numbers);
  }
);

app.put(
  NUMBERS_API_ENDPOINT,
  cors(),
  autneticationMiddleware,
  async (req, res) => {
    const requestBody = req.body;
    let number = {
      name: requestBody.name,
      number: requestBody.number,
      created: new Date(),
      email: req.userEmail
    };
    const savedNumber = await db
      .collection(COLLECTION_NUMBERS)
      .insertOne(number);
    res.status(200).send(savedNumber);
  }
);

app.delete(
  `${NUMBERS_API_ENDPOINT}/:id`,
  cors(),
  autneticationMiddleware,
  async (req, res) => {
    const numberId = req.params.id;
    const result = await db
      .collection(COLLECTION_NUMBERS)
      .deleteOne({ _id: new mongodb.ObjectID(numberId) });
    res.status(200).send({ objectsDeleted: result.deletedCount });
  }
);

app.post("/api/login", cors(), async (req, res) => {
  const requestBody = req.body;
  const existingUser = await db
    .collection(COLLECTION_USERS)
    .findOne({ email: requestBody.email });
  if (existingUser) {
    await db.collection(COLLECTION_USERS).updateOne(
      { _id: new mongodb.ObjectID(existingUser._id) },
      {
        $set: {
          token: requestBody.token,
          imageUrl: requestBody.imageUrl,
          lastLogin: new Date()
        }
      }
    );
  } else {
    requestBody.registered = new Date();
    requestBody.lastLogin = requestBody.registered;
    await db.collection(COLLECTION_USERS).insertOne(requestBody);
  }
  res.status(200).json("OK");
});

mongodb.MongoClient.connect(
  MONGO_DB_URL,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  (err, client) => {
    if (err) return console.log(err);
    db = client.db(MONGO_DATABASE_NAME);
    app.listen(PORT, () => console.log(`Scraper runs on port ${PORT}!`));
  }
);

const TRACKING_URL_POSTE_SRPSKE = "https://www.postesrpske.com/track/tracktracec.php";
const TRACKING_URL_BIH_POSTE = "https://bhpwebout.posta.ba/trackwebapp/";
const TRACKING_URL_X_EXPRESS = "https://www.x-express.ba/korisnicki-panel/pracenje-posiljke"
const TRACKING_URL_EUROEXPRESS = "https://gateway.euroexpress.ba:8083/api/v1/pub/track?trackingNumber="

const config = {
  headers: { "Content-Type": "application/x-www-form-urlencoded" }
};

//////// tracking Poste srpske
const trackPosteSrpske = async trackingNumber => {
  const requestData = { prbr: trackingNumber };
  let trackingResults = [];
  try {
    const response = await axois.post(
      TRACKING_URL_POSTE_SRPSKE,
      querystring.stringify(requestData),
      config
    );
    trackingResults = extractResultsPosteSrpske(response.data);
  } catch (error) {
    console.log(error);
  }
  return trackingResults;
};

//////// tracking BH posta
const trackBihPoste = async trackingNumber => {
  const preRequestDataBihPoste = await prepareRequestForBihPoste();
  const requestDataBihPoste = {
    ...preRequestDataBihPoste,
    tbBrojPosiljke: trackingNumber
  };
  let trackingResults = [];
  try {
    const response = await axois.post(
      TRACKING_URL_BIH_POSTE,
      querystring.stringify(requestDataBihPoste),
      config
    );
    trackingResults = extractResultsBihPoste(response.data);
  } catch (error) {
    console.log(error);
  }
  return trackingResults;
};

const trackXexpress = async trackingNumner => {
  let trackingResults = [];
  
  // for now, do not track if not temu (temu tracking numbers are 19ch long)
  if (trackingNumner.length <= 10) {
    return trackingResults;
  }

  const requestData =  {
      "pracenje_submit": "Pretraga",
      "phone": "1",
      "fax": "2",
      "surname": "x",
      "xexpress1": "3",
      "xexpress2": "3",
      "zbir": "6",
      "pracenje_sifra": trackingNumner
  };

  try {
    const response = await axois.post(
      TRACKING_URL_X_EXPRESS,
      querystring.stringify(requestData),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Cookie": "PH_HPXY_CHECK=s1; path=/;"
       }
      }
    );
    trackingResults = extractResultsXexpress(response.data);
  } catch (error) {
    console.log(error);
  }

  return trackingResults;
}

const trackEuroexpress = async trackingNumner => {
  let trackingResults = [];
  
  if (trackingNumner.length <= 10) {
    return trackingResults;
  }

  try {
    const response = await axois.get(
      TRACKING_URL_EUROEXPRESS + trackingNumner,
      {
        headers: {
          "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/133.0.0.0 Safari/537.36",
       }
      }
    );
    trackingResults = extractResultsEuroexpress(response.data);
  } catch (error) {
    console.log(error);
  }

  return trackingResults;
}

const track = async trackingNumber => {
  if (!trackingNumber) return { message: "There is no tracking number" };

  let trackingResult = {};
  const tarckingResultsBihPoste = await trackBihPoste(trackingNumber);
  const trackingResultsPosteSrpske = await trackPosteSrpske(trackingNumber);
  const trackingResultsXexpress = await trackXexpress(trackingNumber);
  const trackingResultsEuroexpress = await trackEuroexpress(trackingNumber);

  if (tarckingResultsBihPoste && tarckingResultsBihPoste.length) {
    trackingResult = {
      tarckingResultsBihPoste
    };
  }

  if (trackingResultsPosteSrpske && trackingResultsPosteSrpske.length) {
    trackingResult = {
      ...trackingResult,
      trackingResultsPosteSrpske
    };
  }

  if (trackingResultsXexpress && trackingResultsXexpress.length) {
    trackingResult = {
      ...trackingResult,
      trackingResultsXexpress
    };
  }

  if (trackingResultsEuroexpress && trackingResultsEuroexpress.length) {
    trackingResult = {
      ...trackingResult,
      trackingResultsEuroexpress
    };
  }

  return trackingResult;
};

///////// extract data for Poste Srpske
const extractResultsPosteSrpske = data => {
  const results = [];
  const tableRowsResults = $("table > tbody > tr", data);
  for (let i = 1; i < tableRowsResults.length; i++) {
    results.push({
      message: tarnslateToLatin($("td:first-child", tableRowsResults[i]).text()),
      location: tarnslateToLatin($("td:nth-child(2)", tableRowsResults[i]).text()),
      date: tarnslateToLatin($("td:nth-child(3)", tableRowsResults[i]).text())
    });
  }
  return results;
};

////////// extract data for BIH Poste
const extractResultsBihPoste = data => {
  const results = [];
  const tableRowsResult = $("table.tabela tr", data);
  for (let i = 0; i < tableRowsResult.length; i++) {
    results.push({
      message: trimResult($("td:nth-child(4)", tableRowsResult[i]).text()),
      location: trimResult($("td:nth-child(3)", tableRowsResult[i]).text()),
      date: trimResult($("td:nth-child(1)", tableRowsResult[i]).text()),
      country: trimResult($("td:nth-child(2)", tableRowsResult[i]).text())
    });
  }
  return results.filter(isResultSignificant);
};

////////// extract data for X-Express
const extractResultsXexpress = data => {
  const results = [];
  const rowsResults = $("div.pracenje-track-box", data);
  for (let i = 0; i < rowsResults.length; i++) {
    results.push({
      message: tarnslateToLatin($("div.pracenje-track-title", rowsResults[i]).text()),
      date: tarnslateToLatin($("div.pracenje-track-vrijeme", rowsResults[i]).text())
    });
  }
  return results;
};

////////// extract data for Euroexpress
const formatDate = (inputDate) => {
  const date = new Date(inputDate);

  return date.toLocaleString("de-DE", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    hour12: false,
    });
}

const extractResultsEuroexpress = data => {
  if (!data || !data.events || data.events.length == 0) {
    return;
  }

  return data.events.map(trackingEvent => {
    return {
      message: trackingEvent.opis,
      location: trackingEvent.centar,
      date: formatDate(trackingEvent.datum)
    }
  })
};

const isResultSignificant = ({ message, location }) =>
  message &&
  location &&
  location !== "-" &&
  !location.includes("78003") &&
  !(message.includes("Otprema") && message.includes("UVOZ"));

const prepareRequestForBihPoste = async () => {
  let preRequestData = {};
  try {
    const inputElementsFromBihPoste = await getInputElementsFromBihPoste();
    preRequestData = extractDataForRequest(inputElementsFromBihPoste);
  } catch (error) {
    console.log(error);
  }
  return preRequestData;
};

const getInputElementsFromBihPoste = async () => {
  const { data } = await axois.get(TRACKING_URL_BIH_POSTE);
  return $("form input", data);
};

const extractDataForRequest = inputElements => {
  let inputElementsData = {};
  for (let i = 0; i < inputElements.length; i++) {
    const {
      attribs: { name, value }
    } = inputElements[i];
    inputElementsData[name] = value;
  }
  return inputElementsData;
};

const trimResult = value => (value ? value.trim() : "");

const A = {};
A["Ц"]="c";A["У"]="U";A["К"]="K";A["Е"]="E";A["Н"]="N";A["Г"]="G";A["Ш"]="Š";A["З"]="Z";A["Х"]="H";
A["ц"]="c";A["у"]="u";A["к"]="k";A["е"]="e";A["н"]="n";A["г"]="g";A["ш"]="š";A["з"]="z";A["х"]="h";
A["Ф"]="F";A["В"]="V";A["А"]="A";A["П"]="P";A["Р"]="R";A["О"]="O";A["Л"]="L";A["Д"]="D";A["Ж"]="Ž";;A["Ћ"]="Ć";
A["ф"]="f";A["в"]="v";A["а"]="a";A["п"]="p";A["р"]="r";A["о"]="o";A["л"]="l";A["д"]="d";A["ж"]="ž";;A["ћ"]="ć";
A["Ч"]="Č";A["С"]="S";A["М"]="M";A["И"]="I";A["Т"]="T";A["Б"]="B";A["Џ"]="Dž";A["Ђ"]="Đ";A["Љ"]="LJ";A["Њ"]="NJ";
A["ч"]="č";A["с"]="s";A["м"]="m";A["и"]="i";A["т"]="t";A["б"]="b";A["џ"]="dž";A["ђ"]="đ";A["љ"]="lj";A["њ"]="nj";

const tarnslateToLatin = word => {
  if (!word) return word;

  let result = '';
  for(let i = 0; i < word.length; i++) {
      let character = word.charAt(i);
      result += A[character] || character;
  }
  return result;
}